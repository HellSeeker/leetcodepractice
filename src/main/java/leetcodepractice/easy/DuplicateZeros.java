package leetcodepractice.easy;

/*
 * https://leetcode.com/problems/duplicate-zeros/
 * */
public class DuplicateZeros {
	public void duplicateZeros(int[] arr) {
		int l = arr.length - 1;
		for (int i = 0; i < l; i++) {
			if (arr[i] == 0) {
				// int temp = arr[i+1];
				for (int j = l; j > i; j--) {
					arr[j] = arr[j - 1];
				}
				// arr[i+1]=0;
				i++;
			}
		}
	}
}
