package leetcodepractice.easy;

/*
 * https://leetcode.com/problems/palindrome-number/
 * */
public class PalindromeNumber {
	public boolean isPalindrome(int x) {
		int rev = 0;
		int originalNumber = x;
		while (Math.abs(x) != 0) {
			int temp = x % 10;
			rev = rev * 10 + temp;
			x = x / 10;
		}

		if (rev == originalNumber && originalNumber >= 0) {
			return true;
		} else {
			return false;
		}
	}
}
