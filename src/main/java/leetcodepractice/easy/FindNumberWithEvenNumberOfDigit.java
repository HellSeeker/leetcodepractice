package leetcodepractice.easy;

/*
 * https://leetcode.com/problems/find-numbers-with-even-number-of-digits/
 * */
public class FindNumberWithEvenNumberOfDigit {
	public int findNumbers(int[] nums) {
	     
        int res = 0;
        for(int i : nums){
            int count = 0;
            while(i!=0){
                i=i/10;
                count++;
            }
            if(count%2==0){
                res++;
            }
        }
        return res;
    }
}
