package leetcodepractice.easy;

/*
 * https://leetcode.com/problems/length-of-last-word/
 * */
public class LengthOfLastWord {
    public int lengthOfLastWord(String s) {
        String[] stringArray = s.split("\\s");
        return stringArray[stringArray.length-1].length();
    }
    
    public static void main(String... s) {
    	LengthOfLastWord lw = new LengthOfLastWord();
    	System.out.println(lw.lengthOfLastWord("Hello world"));
    }
}
