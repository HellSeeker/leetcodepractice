package leetcodepractice.easy;

/*
 * https://leetcode.com/problems/reverse-integer/
 * */
public class ReverseInteger {
	public int reverse(int x) {
		long ans = 0;
		while (x != 0) {
			int temp = x % 10;
			x = x / 10;
			ans = ans * 10 + temp;
		}
		if (ans < Integer.MIN_VALUE || ans > Integer.MAX_VALUE) {
			return 0;
		} else {
			return (int) ans;
		}
	}
}
