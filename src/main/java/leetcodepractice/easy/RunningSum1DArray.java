package leetcodepractice.easy;

/*
 * https://leetcode.com/problems/running-sum-of-1d-array/
 * */
public class RunningSum1DArray {
	 public int[] runningSum(int[] nums) {
	        int[] res = new int[nums.length];
	        
	        res[0]=nums[0];
	        
	        for(int i = 1; i<nums.length; i++)
	        {
	            res[i]= res[i-1]+nums[i];
	        }
	        
	        return res;
	    }
}
