package leetcodepractice.easy;

/*
 * https://leetcode.com/problems/squares-of-a-sorted-array/
 * */
public class SquareOfSortedArray {
	public int[] sortedSquares(int[] A) {
		for (int i = 0; i < A.length; i++) {
			int temp = A[i];
			A[i] = temp * temp;
		}
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A.length; j++) {
				if (A[i] < A[j]) {
					int temp = A[i];
					A[i] = A[j];
					A[j] = temp;
				}
			}
		}
		return A;
	}
}
