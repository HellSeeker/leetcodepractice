package leetcodepractice.medium;

import java.util.HashSet;
import java.util.Set;

/*
 * https://leetcode.com/problems/find-the-duplicate-number/
 * */
public class FindDuplicateNumber {
	 public int findDuplicate(int[] nums) {
	        Set<Integer> set = new HashSet<> (); 
	        for(int i : nums){
	            if(set.add(i)){}
	            else{
	                return i;
	            }
	        }
	        return -1;
	    }
}
