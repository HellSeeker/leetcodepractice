package leetcodepractice.medium;

/*
 * https://leetcode.com/problems/powx-n/
 * */
public class Pow {
	public double myPow(double x, int n) {
		return Math.pow(x, n);
	}
}
