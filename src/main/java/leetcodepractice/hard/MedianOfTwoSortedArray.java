package leetcodepractice.hard;

import java.util.ArrayList;
import java.util.Collections;

/*
 * https://leetcode.com/problems/median-of-two-sorted-arrays/
 * */
public class MedianOfTwoSortedArray {
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {
		double ans = 0;
		ArrayList<Integer> al = new ArrayList<>();
		for (int i : nums1) {
			al.add(i);
		}
		for (int i : nums2) {
			al.add(i);
		}
		Collections.sort(al);
		int middleIndex = al.size() / 2;
		if (al.size() % 2 == 0) {
			ans = (al.get(middleIndex) + al.get(middleIndex - 1)) / 2.0;
		} else {
			ans = al.get(middleIndex);
		}
		return ans;
	}
}
