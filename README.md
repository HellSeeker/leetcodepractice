# Leetcode Solutions

Leetcode problems solved using Java

## Easy
[Easy Level Solutions](https://gitlab.com/HellSeeker/leetcodepractice/-/tree/main/src/main/java/leetcodepractice/easy)

## Medium
[Medium Level Solutions](https://gitlab.com/HellSeeker/leetcodepractice/-/tree/main/src/main/java/leetcodepractice/medium)

## Hard
[Hard Level Solutions](https://gitlab.com/HellSeeker/leetcodepractice/-/tree/main/src/main/java/leetcodepractice/hard)